import { Component, OnInit, Input } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title: string ;
  @Input() content: string;
  @Input() loveIts;
  @Input() created_at: Date;
  @Input() index: number;

  constructor(private postsService: PostsService, private router : Router) {

  }

  ngOnInit() {

  }

  upVote() {
    this.postsService.upvotePost(this.index);
  }

  downVote() {
    this.postsService.downvotePost(this.index);
  }

  onDeletePost() {
    this.postsService.removePost(this.index);
  }

  getColor(){
    if ( this.loveIts > 0 ){
      return 'green';
    }
    else if ( this.loveIts < 0 ){
      return 'red';
    }
    else {
      return 'grey';
    }
  }
}
