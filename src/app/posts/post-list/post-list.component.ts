import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})

export class PostListComponent implements OnInit, OnDestroy {

  posts: Post[] = [];
  postSubscription = new Subscription;

  constructor(private postsService: PostsService, private router: Router) {

  }

  ngOnInit() {
    this.postSubscription = this.postsService.postSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );

    this.postsService.emitPosts();

  }

  ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }

  onNewPost() {
    this.router.navigate(['/new']);
  }


}
